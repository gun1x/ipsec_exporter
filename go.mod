module gitlab.com/gun1x/ipsec_exporter

go 1.12

require (
	github.com/vishvananda/netlink v1.0.0
	github.com/vishvananda/netns v0.0.0-20180720170159-13995c7128cc
	golang.org/x/sys v0.0.0-20190608050228-5b15430b70e3
	golang.zx2c4.com/wireguard/wgctrl v0.0.0-20190607034155-226bf4e412cd
)
