package main

import (
	"io/ioutil"
	"log"
	"runtime"
	"sync"

	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netns"
	"golang.zx2c4.com/wireguard/wgctrl"
)

var DOCKER_NETNS_PATH = "/var/run/docker/netns/"
var wg sync.WaitGroup

func main() {
	files, err := ioutil.ReadDir(DOCKER_NETNS_PATH)

	if err != nil {
		log.Print("could not open file: ", err)
	} else {
		for _, f := range files {
			namespaceHandle, err := netns.GetFromPath(DOCKER_NETNS_PATH + f.Name())

			if err != nil {
				log.Print("could not open file: ", err)
			} else {
				nChan := make(chan int)
				wg.Add(1)
				go func(ns netns.NsHandle) {
					runtime.LockOSThread()
					netns.Set(ns)
					wgClient, wgctrlErr := wgctrl.New()
					if wgctrlErr != nil {
						log.Println("Wireguard error: ", wgctrlErr)
					}
					d, err := wgClient.Device("wggen4")
					if err != nil {
						wg.Done()
						nChan <- 0
						return
					}
					// here you might want to look for d.Peers where
					// the last handshake is recent enough to count
					n := len(d.Peers)
					nChan <- n
					wg.Done()
				}(namespaceHandle)
				n := <-nChan
				log.Print("the number of wireguard users for network ", f.Name(), " is ", n)

				netlinkHandle, _ := netlink.NewHandleAt(namespaceHandle)
				stateList, _ := netlinkHandle.XfrmStateList(netlink.FAMILY_ALL)
				//// use this in case you need information about the state
				// for i, x := range stateList {
				// 	log.Print("Xfrm state ", i, " is: ", x)
				// }
				n = len(stateList) / 2
				log.Print("The number of IPsec users for network ", f.Name(), " is ", n)
			}
		}
	}
	wg.Wait()
}
